package author

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	pb "go.appointy.com/author/pb"
	"github.com/golang/protobuf/ptypes/empty"
)

var (
	errInternal = status.Error(codes.Internal, `Oops! Something went wrong`) // Generic Error to be returned to client to hide possible sensitive information
)

type authorServer struct {
	//add store
	//add other clients
}

//NewAuthorsServer returns a AuthorsServer implementation with core business logic
func NewAuthorsServer() pb.AuthorsServer {
	return &authorServer{}
}

//CreateAuthor ...
func (s *authorServer) CreateAuthor (ctx context.Context, in *pb.CreateAuthorRequest) ( *pb.Author, error){
	panic(`Implement me`)
}

//GetAuthor ...
func (s *authorServer) GetAuthor (ctx context.Context, in *pb.GetAuthorRequest) ( *pb.Author, error){
	panic(`Implement me`)
}

//UpdateAuthor ...
func (s *authorServer) UpdateAuthor (ctx context.Context, in *pb.UpdateAuthorRequest) ( *empty.Empty, error){
	panic(`Implement me`)
}

//DeleteAuthor ...
func (s *authorServer) DeleteAuthor (ctx context.Context, in *pb.DeleteAuthorRequest) ( *empty.Empty, error){
	panic(`Implement me`)
}

//ListAuthor ...
func (s *authorServer) ListAuthor (ctx context.Context, in *pb.ListAuthorsRequest) ( *pb.ListAuthorsResponse, error){
	panic(`Implement me`)
}

