package authorpb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	driver "go.appointy.com/chaku/driver"
	sql "go.appointy.com/chaku/driver/sql"
	errors "go.appointy.com/chaku/errors"
)

type fieldsToTable map[string]string
type objectTable map[string]fieldsToTable

var objectTableMap = objectTable{
	"author": {
		"id":    "author",
		"name":  "author",
		"email": "author",
		"phone": "author",
	},
}

func (m *Author) PackageName() string {
	return "appointy_author_v1"
}

func (m *Author) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Author) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) ObjectName() string {
	return "author"
}

func (m *Author) Fields() []string {
	return []string{
		"id", "name", "email", "phone",
	}
}

func (m *Author) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Author) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Author) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "name":
		return m.Name, nil
	case "email":
		return m.Email, nil
	case "phone":
		return m.Phone, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "name":
		return &m.Name, nil
	case "email":
		return &m.Email, nil
	case "phone":
		return &m.Phone, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) New(field string) error {
	switch field {
	case "id":
		return nil
	case "name":
		return nil
	case "email":
		return nil
	case "phone":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Author) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "name":
		return "string"
	case "email":
		return "string"
	case "phone":
		return "string"
	default:
		return ""
	}
}

func (_ *Author) GetEmptyObject() (m *Author) {
	m = &Author{}
	return
}

func (m *Author) GetPrefix() string {
	return "aut"
}

func (m *Author) GetID() string {
	return m.Id
}

func (m *Author) SetID(id string) {
	m.Id = id
}

func (m *Author) IsRoot() bool {
	return true
}

func (m *Author) IsFlatObject(f string) bool {
	return false
}

type AuthorStore struct {
	d driver.Driver
}

func NewAuthorStore(d driver.Driver) AuthorStore {
	return AuthorStore{d: d}
}

func NewPostgresAuthorStore(db *x.DB, usr driver.IUserInfo) AuthorStore {
	return AuthorStore{
		&sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
	}
}

func (s AuthorStore) StartTransaction(ctx context.Context) error {
	return s.d.StartTransaction(ctx)
}

func (s AuthorStore) CommitTransaction(ctx context.Context) error {
	return s.d.CommitTransaction(ctx)
}

func (s AuthorStore) RollBackTransaction(ctx context.Context) error {
	return s.d.RollBackTransaction(ctx)
}

func (s AuthorStore) CreateAuthors(ctx context.Context, list ...*Author) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	return s.d.Insert(ctx, vv, &Author{}, &Author{}, "")
}

func (s AuthorStore) DeleteAuthor(ctx context.Context, cond authorCondition) error {
	return s.d.Delete(ctx, cond.authorCondToDriverCond(s.d), &Author{}, &Author{})
}

func (s AuthorStore) UpdateAuthor(ctx context.Context, req *Author, cond authorCondition, fields []string) error {
	return s.d.Update(ctx, cond.authorCondToDriverCond(s.d), req, &Author{}, fields...)
}

func (s AuthorStore) ListAuthors(ctx context.Context, fields []string, cond authorCondition) ([]*Author, error) {
	if len(fields) == 0 {
		fields = (&Author{}).Fields()
	}
	res, err := s.d.Get(ctx, cond.authorCondToDriverCond(s.d), &Author{}, &Author{}, fields...)
	if err != nil {
		return nil, err
	}
	defer res.Close()

	list := make([]*Author, 0, 1000)
	for res.Next(ctx) {
		obj := &Author{}
		if err := res.Scan(obj); err != nil {
			return nil, err
		}
		list = append(list, obj)
	}

	if err := res.Close(); err != nil {
		return nil, err
	}

	return MapperAuthor(list), nil
}

func (s AuthorStore) GetAuthor(ctx context.Context, fields []string, cond authorCondition) (*Author, error) {
	if len(fields) == 0 {
		fields = (&Author{}).Fields()
	}
	objList, err := s.ListAuthors(ctx, fields, cond)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	return objList[0], nil
}

type TrueCondition struct{}

type authorCondition interface {
	authorCondToDriverCond(d driver.Driver) driver.Conditioner
}

type AuthorAnd []authorCondition

func (p AuthorAnd) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.authorCondToDriverCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type AuthorOr []authorCondition

func (p AuthorOr) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.authorCondToDriverCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type AuthorParentEq struct {
	Parent string
}

func (c AuthorParentEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorIdEq struct {
	Id string
}

func (c AuthorIdEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameEq struct {
	Name string
}

func (c AuthorNameEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailEq struct {
	Email string
}

func (c AuthorEmailEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneEq struct {
	Phone string
}

func (c AuthorPhoneEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorIdNotEq struct {
	Id string
}

func (c AuthorIdNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameNotEq struct {
	Name string
}

func (c AuthorNameNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailNotEq struct {
	Email string
}

func (c AuthorEmailNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneNotEq struct {
	Phone string
}

func (c AuthorPhoneNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorIdGt struct {
	Id string
}

func (c AuthorIdGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameGt struct {
	Name string
}

func (c AuthorNameGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailGt struct {
	Email string
}

func (c AuthorEmailGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneGt struct {
	Phone string
}

func (c AuthorPhoneGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorIdLt struct {
	Id string
}

func (c AuthorIdLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameLt struct {
	Name string
}

func (c AuthorNameLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailLt struct {
	Email string
}

func (c AuthorEmailLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneLt struct {
	Phone string
}

func (c AuthorPhoneLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorIdGtOrEq struct {
	Id string
}

func (c AuthorIdGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameGtOrEq struct {
	Name string
}

func (c AuthorNameGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailGtOrEq struct {
	Email string
}

func (c AuthorEmailGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneGtOrEq struct {
	Phone string
}

func (c AuthorPhoneGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorIdLtOrEq struct {
	Id string
}

func (c AuthorIdLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameLtOrEq struct {
	Name string
}

func (c AuthorNameLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailLtOrEq struct {
	Email string
}

func (c AuthorEmailLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneLtOrEq struct {
	Phone string
}

func (c AuthorPhoneLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorDeleted struct{}

func (c AuthorDeleted) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: true, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorNotDeleted struct{}

func (c AuthorNotDeleted) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: false, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedByEq struct {
	By string
}

func (c AuthorCreatedByEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedByNotEq struct {
	By string
}

func (c AuthorCreatedByNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedByGt struct {
	By string
}

func (c AuthorCreatedByGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedByLt struct {
	By string
}

func (c AuthorCreatedByLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedByGtOrEq struct {
	By string
}

func (c AuthorCreatedByGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedByLtOrEq struct {
	By string
}

func (c AuthorCreatedByLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedByEq struct {
	By string
}

func (c AuthorUpdatedByEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedByNotEq struct {
	By string
}

func (c AuthorUpdatedByNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedByGt struct {
	By string
}

func (c AuthorUpdatedByGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedByLt struct {
	By string
}

func (c AuthorUpdatedByLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedByGtOrEq struct {
	By string
}

func (c AuthorUpdatedByGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedByLtOrEq struct {
	By string
}

func (c AuthorUpdatedByLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedByEq struct {
	By string
}

func (c AuthorDeletedByEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedByNotEq struct {
	By string
}

func (c AuthorDeletedByNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedByGt struct {
	By string
}

func (c AuthorDeletedByGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedByLt struct {
	By string
}

func (c AuthorDeletedByLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedByGtOrEq struct {
	By string
}

func (c AuthorDeletedByGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedByLtOrEq struct {
	By string
}

func (c AuthorDeletedByLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Author{}}
}

type AuthorIdIn struct {
	Id []string
}

func (c AuthorIdIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameIn struct {
	Name []string
}

func (c AuthorNameIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailIn struct {
	Email []string
}

func (c AuthorEmailIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneIn struct {
	Phone []string
}

func (c AuthorPhoneIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

type AuthorIdNotIn struct {
	Id []string
}

func (c AuthorIdNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Author{}}
}

type AuthorNameNotIn struct {
	Name []string
}

func (c AuthorNameNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Author{}}
}

type AuthorEmailNotIn struct {
	Email []string
}

func (c AuthorEmailNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Author{}}
}

type AuthorPhoneNotIn struct {
	Phone []string
}

func (c AuthorPhoneNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "phone", Value: c.Phone, Operator: d, Descriptor: &Author{}, FieldMask: "phone", RootDescriptor: &Author{}}
}

func (c TrueCondition) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type authorMapperObject struct {
	id    string
	name  string
	email string
	phone string
}

func (s *authorMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperAuthor(rows []*Author) []*Author {

	combinedAuthorMappers := map[string]*authorMapperObject{}

	for _, rw := range rows {

		tempAuthor := &authorMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempAuthor.id = rw.Id
		tempAuthor.name = rw.Name
		tempAuthor.email = rw.Email
		tempAuthor.phone = rw.Phone

		if combinedAuthorMappers[tempAuthor.GetUniqueIdentifier()] == nil {
			combinedAuthorMappers[tempAuthor.GetUniqueIdentifier()] = tempAuthor
		}
	}

	combinedAuthors := []*Author{}

	for _, author := range combinedAuthorMappers {
		tempAuthor := &Author{}
		tempAuthor.Id = author.id
		tempAuthor.Name = author.name
		tempAuthor.Email = author.email
		tempAuthor.Phone = author.phone

		if tempAuthor.Id == "" {
			continue
		}

		combinedAuthors = append(combinedAuthors, tempAuthor)

	}
	return combinedAuthors
}
