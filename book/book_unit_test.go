package book_test

import (
	mocks "go.appointy.com/book/mocks"
	authorpb "go.appointy.com/book/pb/author"
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"go.appointy.com/book/pb"
	"google.golang.org/genproto/protobuf/field_mask"
	"reflect"
	"strings"
	"testing"
)

func TestBook_Unit_CreateBook(t *testing.T) {

	// Arguments accepted by CreateBook
	type arguments struct {
		ctx context.Context
		req *bookpb.CreateBookRequest
	}

	// What is expected from CreateBook
	type wants struct {
		book *bookpb.Book
		wantErr bool
	}

	b := getBookObject()
	bk := &b

	// array of unit tests to be run
	tests := []struct{

		// name of tests
		name 	string
		// arguments accepted by function
		a 	 	*arguments
		// results expected from function
		w 	 	*wants
		// function to setup tests and mock calls
		setup 	func(*arguments, *mocks.MockAuthorsClient, *wants)
	} {
		{
			name: "Successfully Create Book",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.CreateBookRequest{
					Book: bk,
				},
			},
			w: &wants{
				book: bk,
				wantErr: false,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, w *wants) {

				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(&authorpb.Author{}, nil).Times(1)

			},
		},
		{
			name: "Author Does not exist",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.CreateBookRequest{
					Book: bk,
				},
			},
			w: &wants{
				book: nil,
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, w *wants) {

				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(nil, errors.New("author does not exist")).Times(1)

			},
		},
		{
			name: "Invalid Book",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.CreateBookRequest{
					Book: bk,
				},
			},
			w: &wants{
				book: nil,
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, w *wants) {

				// to make book invalid
				bk.Title = ""

			},
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T){

			ctrl, srv, sm := getMocks(t)
			defer ctrl.Finish()

			tt.setup(tt.a, sm, tt.w)

			got, err := srv.CreateBook(tt.a.ctx, tt.a.req)

			if (err != nil) != tt.w.wantErr {
				t.Errorf("Server.CreateBook() error = %v, wantErr = %v", err, tt.w.wantErr)
				return
			}

			if !tt.w.wantErr {

				tt.w.book.Id = got.Id

				if !reflect.DeepEqual(got, tt.w.book) {
					t.Errorf("Server.CreateBook() = %v, want = %v", got, tt.w.book)
				}
			}
		})
	}
}

func TestBook_Unit_GetBook(t *testing.T) {

	// Arguments accepted by CreateBook
	type arguments struct {
		ctx context.Context
		req *bookpb.GetBookRequest
		book *bookpb.Book
	}

	// What is expected from CreateBook
	type wants struct {
		book *bookpb.Book
		wantErr bool
	}

	b := getBookObject()
	bk := &b

	// array of unit tests to be run
	tests := []struct{

		// name of tests
		name 	string
		// arguments accepted by function
		a 	 	*arguments
		// results expected from function
		w 	 	*wants
		// function to setup tests and mock calls
		setup 	func(*arguments, *mocks.MockAuthorsClient, bookpb.BooksServer, *wants)
	} {
		{
			name: "Successfully Get Book",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.GetBookRequest{
					Id: "", // to be assigned in setup
				},
				book: bk,
			},
			w: &wants{
				book: bk,
				wantErr: false,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {

				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(&authorpb.Author{}, nil).Times(1)

				createdBk, _ := srv.CreateBook(a.ctx, &bookpb.CreateBookRequest{Book: a.book})

				ids := strings.Split(createdBk.Id, "/")

				a.req.Id = ids[len(ids)-1]
				w.book = createdBk

			},
		},
		{
			name: "Book Does not exist",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.GetBookRequest{
					Id: "Unknown ID",
				},
				book: nil,
			},
			w: &wants{
				book: nil,
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {},
		},
		{
			name: "Invalid Request",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.GetBookRequest{
					Id: "", // should not be empty
				},
				book: nil,
			},
			w: &wants{
				book: nil,
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {},
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T){

			ctrl, srv, sm := getMocks(t)
			defer ctrl.Finish()

			tt.setup(tt.a, sm, srv, tt.w)

			got, err := srv.GetBook(tt.a.ctx, tt.a.req)

			if (err != nil) != tt.w.wantErr {
				t.Errorf("Server.CreateBook() error = %v, wantErr = %v", err, tt.w.wantErr)
				return
			}

			if !tt.w.wantErr {
				tt.w.book.Author.Id = got.Author.Id
				if !reflect.DeepEqual(got, tt.w.book) {
					t.Errorf("Server.CreateBook() = %v, want = %v", got, tt.w.book)
				}
			}
		})
	}
}

func TestBook_Unit_UpdateBook(t *testing.T) {

	// Arguments accepted by CreateBook
	type arguments struct {
		ctx context.Context
		req *bookpb.UpdateBookRequest
	}

	// What is expected from CreateBook
	type wants struct {
		wantErr bool
	}

	b := getBookObject()
	bk := &b

	// array of unit tests to be run
	tests := []struct{

		// name of tests
		name 	string
		// arguments accepted by function
		a 	 	*arguments
		// results expected from function
		w 	 	*wants
		// function to setup tests and mock calls
		setup 	func(*arguments, *mocks.MockAuthorsClient, bookpb.BooksServer, *wants)
	} {
		{
			name: "Successfully Update Book",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.UpdateBookRequest{
					Book: bk,
					UpdateMask: &field_mask.FieldMask{Paths: []string{"title"}},
				},
			},
			w: &wants{
				wantErr: false,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {

				// in CreateBook
				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(&authorpb.Author{}, nil).Times(1)

				createdBk, _ := srv.CreateBook(a.ctx, &bookpb.CreateBookRequest{Book: a.req.Book})

				ids := strings.Split(createdBk.Id, "/")

				a.req.Book.Id = ids[len(ids)-1]
				a.req.Book.Title = "Updated Title"

				// in UpdateBook
				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(&authorpb.Author{}, nil).Times(1)

			},
		},
		{
			name: "Author Does not exist",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.UpdateBookRequest{
					Book: bk,
					UpdateMask: &field_mask.FieldMask{Paths: []string{}},
				},
			},
			w: &wants{
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {

				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(nil, errors.New("author does not exist")).Times(1)

			},
		},
		{
			name: "Book Does not exist",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.UpdateBookRequest{
					Book: bk,
					UpdateMask: &field_mask.FieldMask{Paths: []string{}},
				},
			},
			w: &wants{
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {

				bk.Id = "Unknown ID"
			},
		},
		{
			name: "Invalid Book",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.UpdateBookRequest{
					Book: bk,
					UpdateMask: &field_mask.FieldMask{Paths: []string{}},
				},
			},
			w: &wants{
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {

				// to make book invalid
				bk.Title = ""
			},
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T){

			ctrl, srv, sm := getMocks(t)
			defer ctrl.Finish()

			tt.setup(tt.a, sm, srv, tt.w)

			_, err := srv.UpdateBook(tt.a.ctx, tt.a.req)

			if (err != nil) != tt.w.wantErr {
				t.Errorf("Server.CreateBook() error = %v, wantErr = %v", err, tt.w.wantErr)
				return
			}
		})
	}
}

func TestBook_Unit_DeleteBook(t *testing.T) {

	// Arguments accepted by CreateBook
	type arguments struct {
		ctx context.Context
		req *bookpb.DeleteBookRequest
	}

	// What is expected from CreateBook
	type wants struct {
		wantErr bool
	}

	b := getBookObject()
	bk := &b

	// array of unit tests to be run
	tests := []struct{

		// name of tests
		name 	string
		// arguments accepted by function
		a 	 	*arguments
		// results expected from function
		w 	 	*wants
		// function to setup tests and mock calls
		setup 	func(*arguments, *mocks.MockAuthorsClient, bookpb.BooksServer, *wants)
	} {
		{
			name: "Successfully Delete Book",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.DeleteBookRequest{
					Id: "",
				},
			},
			w: &wants{
				wantErr: false,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {

				// in CreateBook
				m.EXPECT().GetAuthor(gomock.Any(), gomock.Any()).Return(&authorpb.Author{}, nil).Times(1)

				createdBk, _ := srv.CreateBook(a.ctx, &bookpb.CreateBookRequest{Book: bk})

				ids := strings.Split(createdBk.Id, "/")

				a.req.Id = ids[len(ids)-1]
			},
		},
		{
			name: "Book Does not exist",
			a: &arguments{
				ctx: context.Background(),
				req: &bookpb.DeleteBookRequest{
					Id: "Unknown Id",
				},
			},
			w: &wants{
				wantErr: true,
			},
			setup: func(a *arguments, m *mocks.MockAuthorsClient, srv bookpb.BooksServer, w *wants) {},
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T){

			ctrl, srv, sm := getMocks(t)
			defer ctrl.Finish()

			tt.setup(tt.a, sm, srv, tt.w)

			_, err := srv.DeleteBook(tt.a.ctx, tt.a.req)

			if (err != nil) != tt.w.wantErr {
				t.Errorf("Server.CreateBook() error = %v, wantErr = %v", err, tt.w.wantErr)
				return
			}
		})
	}
}

