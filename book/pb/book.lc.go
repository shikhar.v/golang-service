// Code generated by protoc-gen-lc, DO NOT EDIT.

package bookpb

import "context"
import "google.golang.org/grpc"

import "github.com/golang/protobuf/ptypes/empty"

type localBooksClient struct {
	BooksServer
}

func NewLocalBooksClient(s BooksServer) BooksClient {
	return &localBooksClient{s}
}

func (lc *localBooksClient) CreateBook(ctx context.Context, in *CreateBookRequest, opts ...grpc.CallOption) (*Book, error) {
	return lc.BooksServer.CreateBook(ctx, in)
}

func (lc *localBooksClient) GetBook(ctx context.Context, in *GetBookRequest, opts ...grpc.CallOption) (*Book, error) {
	return lc.BooksServer.GetBook(ctx, in)
}

func (lc *localBooksClient) UpdateBook(ctx context.Context, in *UpdateBookRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	return lc.BooksServer.UpdateBook(ctx, in)
}

func (lc *localBooksClient) DeleteBook(ctx context.Context, in *DeleteBookRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	return lc.BooksServer.DeleteBook(ctx, in)
}

func (lc *localBooksClient) ListBook(ctx context.Context, in *ListBookRequest, opts ...grpc.CallOption) (*ListBookResponse, error) {
	return lc.BooksServer.ListBook(ctx, in)
}
