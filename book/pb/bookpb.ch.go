package bookpb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	driver "go.appointy.com/chaku/driver"
	sql "go.appointy.com/chaku/driver/sql"
	errors "go.appointy.com/chaku/errors"
)

type fieldsToTable map[string]string
type objectTable map[string]fieldsToTable

var objectTableMap = objectTable{
	"book": {
		"id":           "book",
		"title":        "book",
		"rating":       "book",
		"genre":        "book",
		"publish_date": "book",
		"author":       "author",
	},
	"author": {
		"id":    "author",
		"name":  "author",
		"email": "author",
	},
}

func (m *Book) PackageName() string {
	return "appointy_book_v1"
}

func (m *Book) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Book) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	case "author":
		if m.Author == nil {
			m.Author = &Author{}
		}
		return m.Author, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Book) ObjectName() string {
	return "book"
}

func (m *Book) Fields() []string {
	return []string{
		"id", "title", "rating", "genre", "publish_date", "author",
	}
}

func (m *Book) IsObject(field string) bool {
	switch field {
	case "author":
		return true
	default:
		return false
	}
}

func (m *Book) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Book) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "title":
		return m.Title, nil
	case "rating":
		return m.Rating, nil
	case "genre":
		return m.Genre, nil
	case "publish_date":
		return m.PublishDate, nil
	case "author":
		return m.Author, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Book) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "title":
		return &m.Title, nil
	case "rating":
		return &m.Rating, nil
	case "genre":
		return &m.Genre, nil
	case "publish_date":
		return &m.PublishDate, nil
	case "author":
		return &m.Author, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Book) New(field string) error {
	switch field {
	case "id":
		return nil
	case "title":
		return nil
	case "rating":
		return nil
	case "genre":
		return nil
	case "publish_date":
		if m.PublishDate == nil {
			m.PublishDate = &timestamp.Timestamp{}
		}
		return nil
	case "author":
		if m.Author == nil {
			m.Author = &Author{}
		}
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Book) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "title":
		return "string"
	case "rating":
		return "uint32"
	case "genre":
		return "enum"
	case "publish_date":
		return "timestamp"
	case "author":
		return "message"
	default:
		return ""
	}
}

func (_ *Book) GetEmptyObject() (m *Book) {
	m = &Book{}
	_ = m.New("author")
	m.Author.GetEmptyObject()
	return
}

func (m *Book) GetPrefix() string {
	return "bks"
}

func (m *Book) GetID() string {
	return m.Id
}

func (m *Book) SetID(id string) {
	m.Id = id
}

func (m *Book) IsRoot() bool {
	return true
}

func (m *Book) IsFlatObject(f string) bool {
	return false
}

func (m *Author) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) ObjectName() string {
	return "author"
}

func (m *Author) Fields() []string {
	return []string{
		"id", "name", "email",
	}
}

func (m *Author) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Author) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Author) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "name":
		return m.Name, nil
	case "email":
		return m.Email, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "name":
		return &m.Name, nil
	case "email":
		return &m.Email, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Author) New(field string) error {
	switch field {
	case "id":
		return nil
	case "name":
		return nil
	case "email":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Author) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "name":
		return "string"
	case "email":
		return "string"
	default:
		return ""
	}
}

func (_ *Author) GetEmptyObject() (m *Author) {
	m = &Author{}
	return
}

func (m *Author) GetPrefix() string {
	return "aut"
}

func (m *Author) GetID() string {
	return m.Id
}

func (m *Author) SetID(id string) {
	m.Id = id
}

func (m *Author) IsRoot() bool {
	return false
}

func (m *Author) IsFlatObject(f string) bool {
	return false
}

type BookStore struct {
	d driver.Driver
}

func NewBookStore(d driver.Driver) BookStore {
	return BookStore{d: d}
}

func NewPostgresBookStore(db *x.DB, usr driver.IUserInfo) BookStore {
	return BookStore{
		&sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
	}
}

func (s BookStore) StartTransaction(ctx context.Context) error {
	return s.d.StartTransaction(ctx)
}

func (s BookStore) CommitTransaction(ctx context.Context) error {
	return s.d.CommitTransaction(ctx)
}

func (s BookStore) RollBackTransaction(ctx context.Context) error {
	return s.d.RollBackTransaction(ctx)
}

func (s BookStore) CreateBooks(ctx context.Context, list ...*Book) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	return s.d.Insert(ctx, vv, &Book{}, &Book{}, "")
}

func (s BookStore) DeleteBook(ctx context.Context, cond bookCondition) error {
	return s.d.Delete(ctx, cond.bookCondToDriverCond(s.d), &Book{}, &Book{})
}

func (s BookStore) UpdateBook(ctx context.Context, req *Book, cond bookCondition, fields []string) error {
	return s.d.Update(ctx, cond.bookCondToDriverCond(s.d), req, &Book{}, fields...)
}

func (s BookStore) ListBooks(ctx context.Context, fields []string, cond bookCondition) ([]*Book, error) {
	if len(fields) == 0 {
		fields = (&Book{}).Fields()
	}
	res, err := s.d.Get(ctx, cond.bookCondToDriverCond(s.d), &Book{}, &Book{}, fields...)
	if err != nil {
		return nil, err
	}
	defer res.Close()

	list := make([]*Book, 0, 1000)
	for res.Next(ctx) {
		obj := &Book{}
		if err := res.Scan(obj); err != nil {
			return nil, err
		}
		list = append(list, obj)
	}

	if err := res.Close(); err != nil {
		return nil, err
	}

	return MapperBook(list), nil
}

func (s BookStore) GetBook(ctx context.Context, fields []string, cond bookCondition) (*Book, error) {
	if len(fields) == 0 {
		fields = (&Book{}).Fields()
	}
	objList, err := s.ListBooks(ctx, fields, cond)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	return objList[0], nil
}

type TrueCondition struct{}

type bookCondition interface {
	bookCondToDriverCond(d driver.Driver) driver.Conditioner
}

type BookAnd []bookCondition

func (p BookAnd) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.bookCondToDriverCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type BookOr []bookCondition

func (p BookOr) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.bookCondToDriverCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type BookParentEq struct {
	Parent string
}

func (c BookParentEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookIdEq struct {
	Id string
}

func (c BookIdEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleEq struct {
	Title string
}

func (c BookTitleEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingEq struct {
	Rating uint32
}

func (c BookRatingEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreEq struct {
	Genre Genre
}

func (c BookGenreEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateEq struct {
	PublishDate *timestamp.Timestamp
}

func (c BookPublishDateEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdEq struct {
	Id string
}

func (c BookAuthorIdEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameEq struct {
	Name string
}

func (c BookAuthorNameEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailEq struct {
	Email string
}

func (c BookAuthorEmailEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookIdNotEq struct {
	Id string
}

func (c BookIdNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleNotEq struct {
	Title string
}

func (c BookTitleNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingNotEq struct {
	Rating uint32
}

func (c BookRatingNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreNotEq struct {
	Genre Genre
}

func (c BookGenreNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateNotEq struct {
	PublishDate *timestamp.Timestamp
}

func (c BookPublishDateNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdNotEq struct {
	Id string
}

func (c BookAuthorIdNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameNotEq struct {
	Name string
}

func (c BookAuthorNameNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailNotEq struct {
	Email string
}

func (c BookAuthorEmailNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookIdGt struct {
	Id string
}

func (c BookIdGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleGt struct {
	Title string
}

func (c BookTitleGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingGt struct {
	Rating uint32
}

func (c BookRatingGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreGt struct {
	Genre Genre
}

func (c BookGenreGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateGt struct {
	PublishDate *timestamp.Timestamp
}

func (c BookPublishDateGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdGt struct {
	Id string
}

func (c BookAuthorIdGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameGt struct {
	Name string
}

func (c BookAuthorNameGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailGt struct {
	Email string
}

func (c BookAuthorEmailGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookIdLt struct {
	Id string
}

func (c BookIdLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleLt struct {
	Title string
}

func (c BookTitleLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingLt struct {
	Rating uint32
}

func (c BookRatingLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreLt struct {
	Genre Genre
}

func (c BookGenreLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateLt struct {
	PublishDate *timestamp.Timestamp
}

func (c BookPublishDateLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdLt struct {
	Id string
}

func (c BookAuthorIdLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameLt struct {
	Name string
}

func (c BookAuthorNameLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailLt struct {
	Email string
}

func (c BookAuthorEmailLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookIdGtOrEq struct {
	Id string
}

func (c BookIdGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleGtOrEq struct {
	Title string
}

func (c BookTitleGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingGtOrEq struct {
	Rating uint32
}

func (c BookRatingGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreGtOrEq struct {
	Genre Genre
}

func (c BookGenreGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateGtOrEq struct {
	PublishDate *timestamp.Timestamp
}

func (c BookPublishDateGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdGtOrEq struct {
	Id string
}

func (c BookAuthorIdGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameGtOrEq struct {
	Name string
}

func (c BookAuthorNameGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailGtOrEq struct {
	Email string
}

func (c BookAuthorEmailGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookIdLtOrEq struct {
	Id string
}

func (c BookIdLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleLtOrEq struct {
	Title string
}

func (c BookTitleLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingLtOrEq struct {
	Rating uint32
}

func (c BookRatingLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreLtOrEq struct {
	Genre Genre
}

func (c BookGenreLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateLtOrEq struct {
	PublishDate *timestamp.Timestamp
}

func (c BookPublishDateLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdLtOrEq struct {
	Id string
}

func (c BookAuthorIdLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameLtOrEq struct {
	Name string
}

func (c BookAuthorNameLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailLtOrEq struct {
	Email string
}

func (c BookAuthorEmailLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookDeleted struct{}

func (c BookDeleted) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: true, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookNotDeleted struct{}

func (c BookNotDeleted) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: false, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedByEq struct {
	By string
}

func (c BookCreatedByEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedByNotEq struct {
	By string
}

func (c BookCreatedByNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedByGt struct {
	By string
}

func (c BookCreatedByGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedByLt struct {
	By string
}

func (c BookCreatedByLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedByGtOrEq struct {
	By string
}

func (c BookCreatedByGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedByLtOrEq struct {
	By string
}

func (c BookCreatedByLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookCreatedOnLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedByEq struct {
	By string
}

func (c BookUpdatedByEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedByNotEq struct {
	By string
}

func (c BookUpdatedByNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedByGt struct {
	By string
}

func (c BookUpdatedByGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedByLt struct {
	By string
}

func (c BookUpdatedByLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedByGtOrEq struct {
	By string
}

func (c BookUpdatedByGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedByLtOrEq struct {
	By string
}

func (c BookUpdatedByLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookUpdatedOnLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedByEq struct {
	By string
}

func (c BookDeletedByEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedByNotEq struct {
	By string
}

func (c BookDeletedByNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnNotEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedByGt struct {
	By string
}

func (c BookDeletedByGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnGt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedByLt struct {
	By string
}

func (c BookDeletedByLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnLt) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedByGtOrEq struct {
	By string
}

func (c BookDeletedByGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnGtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedByLtOrEq struct {
	By string
}

func (c BookDeletedByLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c BookDeletedOnLtOrEq) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Book{}, RootDescriptor: &Book{}}
}

type BookIdIn struct {
	Id []string
}

func (c BookIdIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleIn struct {
	Title []string
}

func (c BookTitleIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingIn struct {
	Rating []uint32
}

func (c BookRatingIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreIn struct {
	Genre []Genre
}

func (c BookGenreIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateIn struct {
	PublishDate []*timestamp.Timestamp
}

func (c BookPublishDateIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdIn struct {
	Id []string
}

func (c BookAuthorIdIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameIn struct {
	Name []string
}

func (c BookAuthorNameIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailIn struct {
	Email []string
}

func (c BookAuthorEmailIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

type BookIdNotIn struct {
	Id []string
}

func (c BookIdNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Book{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type BookTitleNotIn struct {
	Title []string
}

func (c BookTitleNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "title", Value: c.Title, Operator: d, Descriptor: &Book{}, FieldMask: "title", RootDescriptor: &Book{}}
}

type BookRatingNotIn struct {
	Rating []uint32
}

func (c BookRatingNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "rating", Value: c.Rating, Operator: d, Descriptor: &Book{}, FieldMask: "rating", RootDescriptor: &Book{}}
}

type BookGenreNotIn struct {
	Genre []Genre
}

func (c BookGenreNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "genre", Value: c.Genre, Operator: d, Descriptor: &Book{}, FieldMask: "genre", RootDescriptor: &Book{}}
}

type BookPublishDateNotIn struct {
	PublishDate []*timestamp.Timestamp
}

func (c BookPublishDateNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "publish_date", Value: c.PublishDate, Operator: d, Descriptor: &Book{}, FieldMask: "publish_date", RootDescriptor: &Book{}}
}

type BookAuthorIdNotIn struct {
	Id []string
}

func (c BookAuthorIdNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "author.id", RootDescriptor: &Book{}}
}

type BookAuthorNameNotIn struct {
	Name []string
}

func (c BookAuthorNameNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "author.name", RootDescriptor: &Book{}}
}

type BookAuthorEmailNotIn struct {
	Email []string
}

func (c BookAuthorEmailNotIn) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "author.email", RootDescriptor: &Book{}}
}

func (c TrueCondition) bookCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type bookMapperObject struct {
	id          string
	title       string
	rating      uint32
	genre       Genre
	publishDate *timestamp.Timestamp
	author      map[string]*bookAuthorMapperObject
}

func (s *bookMapperObject) GetUniqueIdentifier() string {
	return s.id
}

type bookAuthorMapperObject struct {
	id    string
	name  string
	email string
}

func (s *bookAuthorMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperBook(rows []*Book) []*Book {

	combinedBookMappers := map[string]*bookMapperObject{}

	for _, rw := range rows {

		tempBook := &bookMapperObject{}
		tempBookAuthor := &bookAuthorMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		if rw.Author == nil {
			rw.Author = rw.Author.GetEmptyObject()
		}
		tempBookAuthor.id = rw.Author.Id
		tempBookAuthor.name = rw.Author.Name
		tempBookAuthor.email = rw.Author.Email

		tempBook.id = rw.Id
		tempBook.title = rw.Title
		tempBook.rating = rw.Rating
		tempBook.genre = rw.Genre
		tempBook.publishDate = rw.PublishDate
		tempBook.author = map[string]*bookAuthorMapperObject{
			tempBookAuthor.GetUniqueIdentifier(): tempBookAuthor,
		}

		if combinedBookMappers[tempBook.GetUniqueIdentifier()] == nil {
			combinedBookMappers[tempBook.GetUniqueIdentifier()] = tempBook
		} else {

			bookMapper := combinedBookMappers[tempBook.GetUniqueIdentifier()]

			if bookMapper.author[tempBookAuthor.GetUniqueIdentifier()] == nil {
				bookMapper.author[tempBookAuthor.GetUniqueIdentifier()] = tempBookAuthor
			}
			combinedBookMappers[tempBook.GetUniqueIdentifier()] = bookMapper
		}

	}

	combinedBooks := []*Book{}

	for _, book := range combinedBookMappers {
		tempBook := &Book{}
		tempBook.Id = book.id
		tempBook.Title = book.title
		tempBook.Rating = book.rating
		tempBook.Genre = book.genre
		tempBook.PublishDate = book.publishDate

		combinedBookAuthors := []*Author{}

		for _, bookAuthor := range book.author {
			tempBookAuthor := &Author{}
			tempBookAuthor.Id = bookAuthor.id
			tempBookAuthor.Name = bookAuthor.name
			tempBookAuthor.Email = bookAuthor.email

			if tempBookAuthor.Id == "" {
				continue
			}

			combinedBookAuthors = append(combinedBookAuthors, tempBookAuthor)

		}
		if len(combinedBookAuthors) == 0 {
			tempBook.Author = nil
		} else {
			tempBook.Author = combinedBookAuthors[0]

		}

		if tempBook.Id == "" {
			continue
		}

		combinedBooks = append(combinedBooks, tempBook)

	}
	return combinedBooks
}

func (s BookStore) CreateAuthors(ctx context.Context, pid string, list ...*Author) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	return s.d.Insert(ctx, vv, &Author{}, &Book{}, pid)
}

func (s BookStore) DeleteAuthor(ctx context.Context, cond authorCondition) error {
	return s.d.Delete(ctx, cond.authorCondToDriverCond(s.d), &Author{}, &Book{})
}

func (s BookStore) UpdateAuthor(ctx context.Context, req *Author, cond authorCondition, fields []string) error {
	return s.d.Update(ctx, cond.authorCondToDriverCond(s.d), req, &Book{}, fields...)
}

func (s BookStore) ListAuthors(ctx context.Context, fields []string, cond authorCondition) ([]*Author, error) {
	if len(fields) == 0 {
		fields = (&Author{}).Fields()
	}
	res, err := s.d.Get(ctx, cond.authorCondToDriverCond(s.d), &Author{}, &Book{}, fields...)
	if err != nil {
		return nil, err
	}
	defer res.Close()

	list := make([]*Author, 0, 1000)
	for res.Next(ctx) {
		obj := &Author{}
		if err := res.Scan(obj); err != nil {
			return nil, err
		}
		list = append(list, obj)
	}

	if err := res.Close(); err != nil {
		return nil, err
	}

	return MapperAuthor(list), nil
}

func (s BookStore) GetAuthor(ctx context.Context, fields []string, cond authorCondition) (*Author, error) {
	if len(fields) == 0 {
		fields = (&Author{}).Fields()
	}
	objList, err := s.ListAuthors(ctx, fields, cond)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	return objList[0], nil
}

type authorCondition interface {
	authorCondToDriverCond(d driver.Driver) driver.Conditioner
}

type AuthorAnd []authorCondition

func (p AuthorAnd) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.authorCondToDriverCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type AuthorOr []authorCondition

func (p AuthorOr) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.authorCondToDriverCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type AuthorParentEq struct {
	Pid string
}

func (c AuthorParentEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "pid", Value: c.Pid, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorIdEq struct {
	Id string
}

func (c AuthorIdEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameEq struct {
	Name string
}

func (c AuthorNameEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailEq struct {
	Email string
}

func (c AuthorEmailEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorIdNotEq struct {
	Id string
}

func (c AuthorIdNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameNotEq struct {
	Name string
}

func (c AuthorNameNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailNotEq struct {
	Email string
}

func (c AuthorEmailNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorIdGt struct {
	Id string
}

func (c AuthorIdGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameGt struct {
	Name string
}

func (c AuthorNameGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailGt struct {
	Email string
}

func (c AuthorEmailGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorIdLt struct {
	Id string
}

func (c AuthorIdLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameLt struct {
	Name string
}

func (c AuthorNameLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailLt struct {
	Email string
}

func (c AuthorEmailLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorIdGtOrEq struct {
	Id string
}

func (c AuthorIdGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameGtOrEq struct {
	Name string
}

func (c AuthorNameGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailGtOrEq struct {
	Email string
}

func (c AuthorEmailGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorIdLtOrEq struct {
	Id string
}

func (c AuthorIdLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameLtOrEq struct {
	Name string
}

func (c AuthorNameLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailLtOrEq struct {
	Email string
}

func (c AuthorEmailLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorDeleted struct{}

func (c AuthorDeleted) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: true, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorNotDeleted struct{}

func (c AuthorNotDeleted) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: false, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedByEq struct {
	By string
}

func (c AuthorCreatedByEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedByNotEq struct {
	By string
}

func (c AuthorCreatedByNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedByGt struct {
	By string
}

func (c AuthorCreatedByGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedByLt struct {
	By string
}

func (c AuthorCreatedByLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedByGtOrEq struct {
	By string
}

func (c AuthorCreatedByGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedByLtOrEq struct {
	By string
}

func (c AuthorCreatedByLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorCreatedOnLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedByEq struct {
	By string
}

func (c AuthorUpdatedByEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedByNotEq struct {
	By string
}

func (c AuthorUpdatedByNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedByGt struct {
	By string
}

func (c AuthorUpdatedByGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedByLt struct {
	By string
}

func (c AuthorUpdatedByLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedByGtOrEq struct {
	By string
}

func (c AuthorUpdatedByGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedByLtOrEq struct {
	By string
}

func (c AuthorUpdatedByLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorUpdatedOnLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedByEq struct {
	By string
}

func (c AuthorDeletedByEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedByNotEq struct {
	By string
}

func (c AuthorDeletedByNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnNotEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedByGt struct {
	By string
}

func (c AuthorDeletedByGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnGt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedByLt struct {
	By string
}

func (c AuthorDeletedByLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnLt) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedByGtOrEq struct {
	By string
}

func (c AuthorDeletedByGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnGtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedByLtOrEq struct {
	By string
}

func (c AuthorDeletedByLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c AuthorDeletedOnLtOrEq) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Author{}, RootDescriptor: &Book{}}
}

type AuthorIdIn struct {
	Id []string
}

func (c AuthorIdIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameIn struct {
	Name []string
}

func (c AuthorNameIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailIn struct {
	Email []string
}

func (c AuthorEmailIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

type AuthorIdNotIn struct {
	Id []string
}

func (c AuthorIdNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Author{}, FieldMask: "id", RootDescriptor: &Book{}}
}

type AuthorNameNotIn struct {
	Name []string
}

func (c AuthorNameNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "name", Value: c.Name, Operator: d, Descriptor: &Author{}, FieldMask: "name", RootDescriptor: &Book{}}
}

type AuthorEmailNotIn struct {
	Email []string
}

func (c AuthorEmailNotIn) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "email", Value: c.Email, Operator: d, Descriptor: &Author{}, FieldMask: "email", RootDescriptor: &Book{}}
}

func (c TrueCondition) authorCondToDriverCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type authorMapperObject struct {
	id    string
	name  string
	email string
}

func (s *authorMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperAuthor(rows []*Author) []*Author {

	combinedAuthorMappers := map[string]*authorMapperObject{}

	for _, rw := range rows {

		tempAuthor := &authorMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempAuthor.id = rw.Id
		tempAuthor.name = rw.Name
		tempAuthor.email = rw.Email

		if combinedAuthorMappers[tempAuthor.GetUniqueIdentifier()] == nil {
			combinedAuthorMappers[tempAuthor.GetUniqueIdentifier()] = tempAuthor
		}
	}

	combinedAuthors := []*Author{}

	for _, author := range combinedAuthorMappers {
		tempAuthor := &Author{}
		tempAuthor.Id = author.id
		tempAuthor.Name = author.name
		tempAuthor.Email = author.email

		if tempAuthor.Id == "" {
			continue
		}

		combinedAuthors = append(combinedAuthors, tempAuthor)

	}
	return combinedAuthors
}
