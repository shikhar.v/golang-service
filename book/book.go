package book

import (
	"context"
	"go.appointy.com/chaku/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	pb "go.appointy.com/book/pb"
	"github.com/golang/protobuf/ptypes/empty"
	"strings"

	authorpb "go.appointy.com/book/pb/author"
)

var (
	errInternal = status.Error(codes.Internal, `Oops! Something went wrong`) // Generic Error to be returned to client to hide possible sensitive information
)

type bookServer struct {
	//add store
	store pb.BookStore
	//add other clients
	authorCli authorpb.AuthorsClient
}

//NewBooksServer returns a BooksServer implementation with core business logic
func NewBooksServer(Store pb.BookStore, aCli authorpb.AuthorsClient) pb.BooksServer {
	return &bookServer{store: Store, authorCli: aCli}
}

//CreateBook ...
func (s *bookServer) CreateBook (ctx context.Context, in *pb.CreateBookRequest) ( *pb.Book, error){

	// Validate Request
	err := in.Validate()
	if err != nil {
		return nil, status.Error(codes.FailedPrecondition, err.Error())
	}

	// Business Logic Here

	// Check if author exists
	_, err = s.authorCli.GetAuthor(ctx, &authorpb.GetAuthorRequest{Id: in.Book.Author.Id})
	if err != nil {
		// No need to log here as already logged in client from which function is called
		return nil, err
	}

	// Contact store for Create Operation
	ids, err := s.store.CreateBooks(ctx, in.Book)
	if err != nil{
		// log error here
		return nil, errInternal
	}

	in.Book.Id = ids[0]

	return in.Book, nil}

//GetBook ...
func (s *bookServer) GetBook (ctx context.Context, in *pb.GetBookRequest) ( *pb.Book, error){

	// Validate Request
	err := in.Validate()
	if err != nil {
		return nil, status.Error(codes.FailedPrecondition, err.Error())
	}

	// Business Logic Here

	// Contact store for Get Operation
	book, err := s.store.GetBook(ctx, []string{}, pb.BookAnd{pb.BookIdEq{in.Id}, pb.BookNotDeleted{}})
	if err != nil {
		if err == errors.ErrNotFound{
			return nil, status.Error(codes.NotFound, "Book Not Found")
		}
		// log error here
		return nil, errInternal
	}

	return book, nil
}

//UpdateBook ...
func (s *bookServer) UpdateBook (ctx context.Context, in *pb.UpdateBookRequest) ( *empty.Empty, error){

	// Validate Request
	err := in.Validate()
	if err != nil {
		return nil, status.Error(codes.FailedPrecondition, err.Error())
	}

	// Business Logic Here

	// Check if author exists
	_, err = s.authorCli.GetAuthor(ctx, &authorpb.GetAuthorRequest{Id: in.Book.Author.Id})
	if err != nil {
		// No need to log here as already logged in client from which function is called
		return nil, err
	}

	// Get book ID (ID: parentID/bookID)
	ids := strings.Split(in.Book.Id, "/")
	bookId := ids[len(ids)-1]

	// Check if book exists
	_, err = s.GetBook(ctx, &pb.GetBookRequest{Id: bookId})
	if err != nil {
		return nil, err
	}

	// Contact store for Update Operation
	err = s.store.UpdateBook(ctx, in.Book, pb.BookIdEq{bookId}, in.UpdateMask.Paths)
	if err != nil {
		// log error here
		return nil, errInternal
	}

	return &empty.Empty{}, nil
}

//DeleteBook ...
func (s *bookServer) DeleteBook (ctx context.Context, in *pb.DeleteBookRequest) ( *empty.Empty, error){

	// Validate Request
	err := in.Validate()
	if err != nil {
		return nil, status.Error(codes.FailedPrecondition, err.Error())
	}

	// Business Logic Here

	// Check if book exists
	_, err = s.GetBook(ctx, &pb.GetBookRequest{Id: in.Id})
	if err != nil {
		return nil, err
	}

	// Contact store for Delete Operation
	err = s.store.DeleteBook(ctx, pb.BookIdEq{in.Id})
	if err != nil {
		// log error here
		return nil, errInternal
	}

	return &empty.Empty{}, nil
}

//ListBook ...
func (s *bookServer) ListBook (ctx context.Context, in *pb.ListBookRequest) ( *pb.ListBookResponse, error){

	// Validate Request
	err := in.Validate()
	if err != nil {
		return nil, status.Error(codes.FailedPrecondition, err.Error())
	}

	// Business Logic Here

	// Contact store for List Operation
	books, err := s.store.ListBooks(ctx, []string{}, pb.TrueCondition{})
	if err != nil {
		// log error here
		return nil, errInternal
	}

	return &pb.ListBookResponse{Books: books}, nil
}

