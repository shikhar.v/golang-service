module go.appointy.com/book

require (
	github.com/elgris/sqrl v0.0.0-20181124135704-90ecf730640a
	github.com/golang/protobuf v1.3.1
	github.com/lyft/protoc-gen-validate v0.0.14
	go.appointy.com/chaku v0.0.0-20190511110620-0c28cb3b9df4
	go.appointy.com/events v0.0.0-20190511104427-1439c5a89828
	go.appointy.com/pehredaar v0.0.0-20190511104434-58da09d1df3c
	go.appointy.com/waqt/protos v0.0.0-20190511104255-02410dea2c8c
	go.uber.org/fx v1.9.0
	google.golang.org/appengine v1.4.0 // indirect
	google.golang.org/genproto v0.0.0-20190508193815-b515fa19cec8
	google.golang.org/grpc v1.20.1
)
